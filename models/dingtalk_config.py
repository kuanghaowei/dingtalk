from odoo import models, fields


class DingtalkConfig(models.Model):
    _name = 'dingtalk.config'
    _description = '应用配置'

    name = fields.Char('应用名称')
    appkey = fields.Char('AppKey')
    appsecret = fields.Char('AppSecret')
    company_id = fields.Many2one('res.company', string='公司',  default=lambda self: self.env.company)

    # token = fields.Char(string='随机Token')
    # encoding_aes_key = fields.Char('EncodingAESKey')
    agent_id = fields.Char('应用AgentId')

